name := "CMS-Shared"

lazy val buildSettings = Seq(
	organization := "xyz.whynotzoidberg",
	scalaVersion := "2.12.5",
	sbtVersion := "1.1.0",
	version := "0.0.1-SNAPSHOT"
)

lazy val root = project.in(file(".")).settings(
	buildSettings
).aggregate(sharedMain, testModule)

lazy val sharedMain = module("SharedMain")
	.settings(
		buildSettings,
		libraryDependencies ++= Seq(
			
		)
	)
	
lazy val testModule = module("Tests")
	.settings(
		buildSettings,
		libraryDependencies ++= Seq(
			
		)
	)

def module(name: String) = Project(name, file(s"modules/$name"))